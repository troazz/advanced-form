<?php

namespace Qareer\AdvancedForm;

/**
 * Class Field
 *
 * @package Qareer\system\shared\libraries\form
 */
abstract class Field
{
    /** @var Form */
    protected $form;

    /** @var string */
    protected $id;

    /** @var string */
    protected $name;

    /** @var mixed */
    protected $value;

    /** @var Eloquent Model */
    protected $boundModel;

    /** @var mixed */
    protected $boundField;

    /** @var array */
    protected $classes = [];

    /** @var array */
    protected $attributes = [];

    /** @var array */
    protected $rules = [];

    /** @var \Closure */
    protected $valueModifier;

    /** @var string */
    protected $label;

    /** @var bool */
    protected $isRendered = false;

    /** @var bool */
    protected $isDisabled = false;

    /** @var bool */
    protected $isArray = false;

    /** @var string */
    protected $arrayKey;

    public function __construct(Form $form, $name, $value = null)
    {
        $this->form     = $form;
        $this->id       = $name;
        $this->name     = $name;
        $this->value    = $value;
        $this->label    = ucwords(str_replace('_', ' ', $name));
    }

    /**
     * Get field base name without array notation
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get input full name (array notation is included)
     *
     * @return string
     */
    public function getFullName()
    {
        return ($this->isArray)
            ? $this->getName()."[{$this->arrayKey}]"
            : $this->getName();
    }

    /**
     * Set this field in array mode
     *
     * Setting a field in array mode will affect it's rendered name
     *
     * @param string $id
     *
     * @return $this
     */
    public function setAsArray($id)
    {
        $this->isArray  = true;
        $this->arrayKey = $id;

        return $this;
    }

    public function getArrayKey()
    {
        return $this->arrayKey;
    }

    /**
     * Get bound value from model
     *
     * @return mixed
     */
    public function getBoundValue()
    {
        $boundValue = null;

        $hasPostValue = $this->form->getPostValue($this->getName());

        if ($this->isBound() && !$hasPostValue) {
            if (is_callable($this->boundField)) {
                $boundValue = call_user_func($this->boundField, $this->boundModel);
            } else {
                $boundValue = $this->boundModel->getAttribute($this->boundField);
            }
        }

        return $boundValue;
    }

    /**
     * Check if the model is bound by a model and it's attribute
     *
     * @return bool
     */
    public function isBound()
    {
        return $this->boundModel !== null
            && $this->boundField !== null;
    }

    /**
     * Set the input bound model
     *
     * @param Eloquent $model
     *
     * @return $this
     */
    public function setBoundModel($model)
    {
        $this->boundModel = $model;

        return $this;
    }

    /**
     * Get input value
     *
     * @return mixed
     */
    public function getValue()
    {
        $value = $this->getBoundValue() ?: $this->value;

        if ($this->valueModifier) {
            $value = call_user_func_array($this->valueModifier, [
                $value,
                $this->boundModel,
                $this->boundField
            ]);
        }

        return $value;
    }

    /**
     * Set the input bound field
     *
     * @param string|\Closure $field
     *
     * @return $this
     */
    public function setBoundField($field)
    {
        $this->boundField = $field;

        return $this;
    }

    /**
     * Get the input bound field
     *
     * @return string|\Closure
     */
    public function getBoundField()
    {
        return $this->boundField;
    }

    /**
     * Set field value
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Set field value if null
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function setValueIfNull($value)
    {
        if ($this->value == NULL) {
            $this->value = $value;
        }
        return $this;
    }

    /**
     * Set value modifier to be called on render
     *
     * @param callable $modifier
     *
     * @return $this
     */
    public function setValueModifier(\Closure $modifier)
    {
        $this->valueModifier = $modifier;

        return $this;
    }

    /**
     * Add a new rule for this field
     *
     * @param string|\Closure|callable  $rule
     *
     * @return $this
     */
    public function addRule()
    {
        $this->rules[] = func_get_args();

        return $this;
    }

    /**
     * Get field rules
     *
     * @return array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Set the input id
     *
     * @param string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the input id
     *
     * @return string
     */
    public function getId()
    {
        $prefix = $this->form->getFieldIdPrefix() ? $this->form->getFieldIdPrefix().'.' : '';

        if ($this->isArray) {
            $id = $prefix.$this->id."[{$this->arrayKey}]";
        } else {
            $id = $prefix.$this->id;
        }

        return $id;
    }

    /**
     * Set the input classes
     *
     * @param array $classes
     *
     * @return $this
     */
    public function setClasses(array $classes)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Get the input classes
     *
     * @return array
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Add a single input class
     *
     * @param string $class
     *
     * @return $this
     */
    public function addClass($class)
    {
        $this->classes[] = $class;

        return $this;
    }

    /**
     * Add a single key value html attribute
     *
     * @param string    $name
     * @param mixed     $value
     *
     * @return $this
     */
    public function addAttribute($name, $value)
    {
        if (!in_array($name, ['name', 'id', 'disabled', 'value', 'checked'])) {
            if ($name == 'class'){
                $this->addClass($value);
            } elseif ($value === null) {
                $this->attributes[$name] = null;
            } else {    
                if (!isset($this->attributes[$name])) {
                    $this->attributes[$name] = [];
                }

                $this->attributes[$name] = [$value];
            }
        }

        return $this;
    }

    /**
     * Override a single key value input attribute
     *
     * @param string    $name
     * @param mixed     $value
     *
     * @return $this
     */
    public function setAttribute($name, $value) {
        if ($name == 'class') {
            $this->setClasses(explode(' ', $value));
        } else {
            $this->attributes[$name] = $value;
        }

        return $this;
    }

    /**
     * Override multiple key value input attribute
     *
     * @param array     $attributes
     *
     * @return $this
     */
    public function setAttributes($attributes) {
        foreach ($attributes as $name => $value) {
            $this->setAttribute($name, $value);
        }

        return $this;
    }

    /**
     * Add a data attribute
     *
     * @param string    $name
     * @param string    $value
     *
     * @return Field
     */
    public function addDataAttribute($name, $value)
    {
        return $this->addAttribute('data-'.$name, $value);
    }

    /**
     * Set field as autocomplete
     *
     * @param string $source
     *
     * @return Field
     */
    public function setAutocomplete($source)
    {
        return $this->addAttribute('qf-autocomplete', $source);
    }

    /**
    * Set field as autocorrect
    *
    * @param string $source
    *
    * @return Field     
    */
    public function setAutocorrect($source)
    {
        return $this->addAttribute('qf-autocorrect', $source)->addAttribute('ng-value', "value");
    }
    

    /**
     * Check is the input is disabled
     *
     * @return bool
     */
    public function isDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * Set the input disabled attribute
     *
     * @param bool $disabled
     *
     * @return $this
     */
    public function setDisabled($disabled = true)
    {
        $this->isDisabled = $disabled;

        return $this;
    }

    /**
     * Renders the input html if not self closing
     *
     * @return string
     */
    protected function renderInner()
    {
        return '';
    }

    /**
     * Renders the complete HTML element tag
     *
     * @return string
     */
    public function renderElement()
    {
        $inputType    = $this->isSelfClosing() ? 'input' : $this->getType();
        $currentClass = last(explode('\\', get_class($this)));
        if ($this->form->usingJavascriptValidation() && !in_array($currentClass, $this->form->clientUnvalidatableField)) {
            $hasRule = false;
            foreach($this->getRules() as $rule) {
                switch (g($rule, 0)) {
                    case 'required':
                        $hasRule = true;
                        $this->addAttribute('required', null);
                        break;
                    case 'url':
                        $hasRule = true;
                        $this->addAttribute('type', 'url');
                    case 'lengthMax': 
                        $hasRule = true;
                        $this->addAttribute('maxlength', g($rule, 1));
                        break;
                    case 'lengthMin': 
                        $hasRule = true;
                        $this->addAttribute('minlength', g($rule, 1));
                        break;
                    case 'numeric':
                        $hasRule = true;
                        $this->addAttribute('type', 'number');
                        break;
                    case 'min':
                        $hasRule = true;
                        $this->addAttribute('type', 'number');
                        $this->addAttribute('min', g($rule, 1));
                        break;
                    case 'max': 
                        $hasRule = true;
                        $this->addAttribute('type', 'number');
                        $this->addAttribute('max', g($rule, 1));
                        break;
                }
            }
            if ($hasRule) {
                $this->addAttribute('v-model', "model.".$this->getName());
                $this->addAttribute('v-form-ctrl', null);
            }
        }
        $stringAttributes = $this->parseAttributesAsString();
        
        if ($this->isSelfClosing()) {
            $inputString = "<{$inputType} {$stringAttributes} />";
        } else {
            $inputString = "<{$inputType} {$stringAttributes}>{$this->renderInner()}</{$inputType}>";
        }

        return $inputString;
    }

    /**
     * Renders the input
     *
     * @return string
     */
    public function render()
    {
        $inputString = $this->renderElement();

        $this->isRendered   = true;

        return $inputString;
    }

    /**
     * Parse attribute as string
     *
     * @return string
     */
    protected function parseAttributesAsString()
    {
        return $this->form->parseAttributesAsString(array_merge(['type' => $this->getType()], $this->getAttributes(), [
            'disabled'  => $this->isDisabled(),
            'class'     => implode(' ', $this->getClasses())
        ]));
    }

    /**
     * Check the existence of an attribute
     *
     * @param string $attribute
     *
     * @return bool
     */
    public function hasAttribute($attribute)
    {
        return isset($this->attributes[$attribute]);
    }

    /**
     * Get the input attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        $attributes = $this->attributes;

        if (!isset($this->attributes['id'])) {
            $attributes['id'] = $this->getId();
        }

        if (!isset($this->attributes['value'])) {
            $attributes['value'] = $this->getValue();
        }

        if (!isset($this->attributes['name'])) {
            $attributes['name'] = $this->getFullName();
        }

        if (!isset($this->attributes['disabled'])) {
            $attributes['disabled'] = $this->isDisabled();
        }

        return $attributes;
    }

    public function handle($postValue)
    {
        $this->setValue($postValue);

        return $this->value;
    }

    /**
     * Get the input type
     *
     * @return string
     */
    abstract public function getType();

    /**
     * Check is the input tag is self closing
     *
     * @return boolean
     */
    abstract public function isSelfClosing();

    /**
     * @param $text
     * @return Field
     */
    public function setPlaceholder($text)
    {
        return $this->addAttribute('placeholder', $text);
    }


    /** {@inheritDoc} */
    public function __invoke()
    {
        return call_user_func_array([$this, 'render'], func_get_args());
    }

    /** {@inheritDoc} */
    public function __toString()
    {
        return $this->__invoke();
    }

    /**
     * @param $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * get label with it's wrapper
     * @param  array  $attributes
     * @return string
     */
    public function label($attributes = [])
    {
        $stringAttributes = $this->form->parseAttributesAsString($attributes);
        $trans = [
            ':label' => $this->label, 
            ':for' => $this->id, 
            ':attributes' => $stringAttributes 
        ];
        $labelWrapper = ($this->form->labelWrapper) ?: $this->form->getDefaultLabelWrapper();
        
        return strtr($labelWrapper, $trans);
    }

}