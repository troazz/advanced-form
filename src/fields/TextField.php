<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class TextField
 *
 * @package Qareer\AdvancedForm\fields
 */
class TextField extends Field
{
    /** {@inheritDoc} */
    public function getType()
    {
        return 'text';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return true;
    }
}