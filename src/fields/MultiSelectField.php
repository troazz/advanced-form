<?php

namespace Qareer\AdvancedForm\Fields;

/**
 * Class MultipleSelectField
 *
 * @package Qareer\AdvancedForm\Fields
 */
class MultiSelectField extends SelectField
{
    /** {@inheritDoc} */
    protected function renderInner()
    {
        $inputValues = (array)$this->getValue();

        $loopOptions = function ($options) use (&$loopOptions, $inputValues) {
            $stringOption = '';

            foreach ($options as $key => $value) {
                $key = $this->form->html_escape($key);

                if (is_array($value)) {
                    $stringOption .= "<optgroup label='{$key}'>{$loopOptions($value)}</optgroup>";
                } else {
                    $value          = $this->form->html_escape($value);

                    if ($this->valueModifier) {
                        $value = call_user_func($this->valueModifier, $value);
                    }

                    $attributes     = $this->form->parseAttributesAsString([
                        'selected'  => in_array($key, $inputValues),
                        'value'     => $key
                    ]);
                    $stringOption   .= "<option {$attributes}>{$value}</option>";
                }
            }

            return $stringOption;
        };

        return $loopOptions($this->options);
    }

    /** {@inheritDoc} */
    public function getAttributes()
    {
        $attributes = parent::getAttributes();
        $attributes['multiple'] = '';

        // select element does not require a value attribute
        if (isset($attributes['value'])) {
            unset($attributes['value']);
        }

        return $attributes;
    }

    public function getFullName()
    {
        if( strpos($this->name, '[]') != false ){
            return $this->name;
        }
        return $this->name.'[]';
    }

    public function handle($postValue)
    {
        $this->setValue($postValue);

        return $this->value;
    }


}