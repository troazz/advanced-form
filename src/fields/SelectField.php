<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class SelectField
 *
 * @package Qareer\AdvancedForm\Fields
 */
class SelectField extends Field
{
    protected $options;

    public function setOptions(array $options = [])
    {
        $this->options = $options;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    /** {@inheritDoc} */
    protected function renderInner()
    {
        if (!is_array($this->getValue())) {
            $inputValue = (string)$this->getValue();
            $inputValue = explode('|', $inputValue);
        } else {
            $inputValue = $this->getValue();
        }

        $loopOptions = function ($options) use (&$loopOptions, $inputValue) {
            $stringOption = '';

            foreach ($options as $key => $value) {
                $key = $this->form->html_escape($key);

                if (is_array($value)) {
                    $stringOption .= "<optgroup label='{$key}'>{$loopOptions($value)}</optgroup>";
                } else {
                    $value          = $this->form->html_escape($value);

                    if ($this->valueModifier) {
                        $value = call_user_func($this->valueModifier, $value);
                    }

                    $attributes     = $this->form->parseAttributesAsString([
                        'selected'  => in_array($key, $inputValue),
                        'value'     => $key
                    ]);
                    $stringOption   .= "<option {$attributes}>{$value}</option>";
                }
            }

            return $stringOption;
        };

        return $loopOptions($this->options);
    }

    /** {@inheritDoc} */
    public function getValue()
    {
        return $this->getBoundValue() ?: $this->value;
    }

    /** {@inheritDoc} */
    public function getAttributes()
    {
        $attributes = parent::getAttributes();

        // select element does not require a value attribute
        if (isset($attributes['value'])) {
            $attributes['selected-value'] = $attributes['value'];
            unset($attributes['value']);
        }

        return $attributes;
    }

    /** {@inheritDoc} */
    public function getType()
    {
        return 'select';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return false;
    }
}