<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;
use Qareer\AdvancedForm\Form;

/**
 * Class RadioField
 *
 * @package Qareer\AdvancedForm\Fields
 */
class RadioField extends Field
{
    protected $unchecked;

    public function __construct(Form $form, $name, $value)
    {
        parent::__construct($form, $name, $value);
    }

    /** {@inheritDoc} */
    public function getType()
    {
        return 'radio';
    }

    /** {@inheritDoc} */
    public function getAttributes()
    {
        $attributes = parent::getAttributes();

        if(!g($attributes, 'checked')){
            if ($this->isBound()) {
                $attributes['checked'] = $this->getValue() == $this->getBoundValue();
            } else {
                $attributes['checked'] = $this->getValue() == $this->form->getPostValue($this->getName());
            }
        }

        if($this->unchecked){
            unset($attributes['checked']);
        }

        return $attributes;
    }

    /** {@inheritDoc} */
    public function setValue($value)
    {
        $this->attributes['checked'] = ($this->value == $value);

        return $this;
    }

    /** {@inheritDoc} */
    public function handle($postValue)
    {
        return ($this->getValue() == $postValue)
            ? $this->getValue() : false;
    }

    /** {@inheritDoc} */
    public function getValue()
    {
        return $this->value;
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return true;
    }

    /** {@inheritDoc} */
    public function unchecked()
    {
        return $this->unchecked = true;
    }
}