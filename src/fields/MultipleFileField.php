<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class MultipleFileField
 *
 * @package Qareer\AdvancedForm\fields
 */
class MultipleFileField extends Field {
    private $showFilePath = false;
    private $fileValues;

    /**
     * If this is set to true, rendered element will have an extra
     * attribute "data-filepath='...'"
     *
     * @param bool $show
     *
     * @return $this
     */
    public function setFilePathShown($show = false)
    {
        $this->showFilePath = $show;

        return $this;
    }

    /** {@inheritDoc} */
    public function getAttributes()
    {
        $attributes = parent::getAttributes();

        if ($this->showFilePath) {
            $attributes['data-filepath'] = html_entity_decode($this->getBoundValue(), ENT_QUOTES);
        }

        $attributes['multiple'] = '1';
        $attributes['value'] = false;

        return $attributes;
    }

    /** {@inheritDoc} */
    public function handle($postValue)
    {
        if ($this->isArray) {
            $arrayName = [];

            parse_str(http_build_query([$this->name => '']), $arrayName);

            $size = sizeof(g($_FILES, g(array_keys($arrayName), 0), []));

            $this->fileValues = [];
            for ($i = 0; $i < $size; $i++) {
                $fieldData = [];
                $fileField =  g($_FILES, g(array_keys($arrayName), 0), []);

                foreach ($fileField as $type => $fieldNames) {
                    $fieldRealName      = g(array_keys(reset($arrayName)), 0);
                    $fieldData[$type]   = g(g(g($fieldNames, $fieldRealName), $i), $this->arrayKey);
                }

                $itemData = array_map(function ($fileData) {
                    return g($fileData, $this->arrayKey);
                }, $fieldData);

                $this->fileValues[] = $itemData;
            }
        } else {
            $postInput = g($_FILES, str_replace('[]', '', $this->getFullName()));
            $size = count($postInput["name"]);

            $result = [];
            for ($i = 0; $i < $size; $i++) {
                $file = [];
                foreach (array_keys($postInput) as $key) {
                    $file[$key] = $postInput[$key][$i];
                }
                $result[] = $file;
            }
            $this->fileValues = $result;
        }

        return $this->fileValues;
    }

    /** {@inheritDoc} */
    public function getType()
    {
        return 'file';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return true;
    }
}