<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class PasswordField
 *
 * @package Qareer\AdvancedForm\fields
 */
class PasswordField extends Field
{
    /** {@inheritDoc} */
    public function getType()
    {
        return 'password';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return true;
    }
}