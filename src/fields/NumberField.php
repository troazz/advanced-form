<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class NumberField
 *
 * @package Qareer\AdvancedForm\fields
 */
class NumberField extends Field
{
    /** {@inheritDoc} */
    public function getType()
    {
        return 'number';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return true;
    }
}