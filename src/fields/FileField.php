<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class FileField
 *
 * @package Qareer\AdvancedForm\fields
 */
class FileField extends Field
{
    private $showFilePath = false;
    private $fileValues;

    /**
     * If this is set to true, rendered element will have an extra
     * attribute "data-filepath='...'"
     *
     * @param bool $show
     *
     * @return $this
     */
    public function setFilePathShown($show = false)
    {
        $this->showFilePath = $show;

        return $this;
    }

    /** {@inheritDoc} */
    public function getAttributes()
    {
        $attributes = parent::getAttributes();

        if ($this->showFilePath) {
            $attributes['data-filepath'] = html_entity_decode($this->getBoundValue(), ENT_QUOTES);
        }

        $attributes['value'] = false;

        return $attributes;
    }

    /** {@inheritDoc} */
    public function handle($postValue)
    {
        if ($this->isArray) {
            $arrayName = [];

            parse_str(http_build_query([$this->name => '']), $arrayName);

            $fieldData = [];
            $fileField =  g($_FILES, g(array_keys($arrayName), 0), []);

            foreach ($fileField as $type => $fieldNames) {
                $fieldRealName      = g(array_keys(reset($arrayName)), 0);
                $fieldData[$type]   = g(g($fieldNames, $fieldRealName), $this->arrayKey);
            }

            $itemData = array_map(function ($fileData) {
                return g($fileData, $this->arrayKey);
            }, $fieldData);

            $this->fileValues = $itemData;
        } else {
            $this->fileValues = g($_FILES, $this->getFullName());
        }

        return $this->fileValues;
    }

    /** {@inheritDoc} */
    public function getType()
    {
        return 'file';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return true;
    }
}