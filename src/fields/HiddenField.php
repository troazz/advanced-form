<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class HiddenField
 *
 * @package Qareer\AdvancedForm\fields
 */
class HiddenField extends Field
{
    /** {@inheritDoc} */
    public function getType()
    {
        return 'hidden';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return true;
    }
}