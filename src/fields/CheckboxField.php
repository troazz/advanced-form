<?php

namespace Qareer\AdvancedForm\Fields;

/**
 * Class CheckboxField
 *
 * @package Qareer\AdvancedForm\Fields
 */
class CheckboxField extends RadioField
{
    /** {@inheritDoc} */
    public function getType()
    {
        return 'checkbox';
    }
}