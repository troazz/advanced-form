<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class EmailField
 *
 * @package Qareer\AdvancedForm\fields
 */
class EmailField extends Field
{
    /** {@inheritDoc} */
    public function getType()
    {
        return 'email';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return true;
    }
}