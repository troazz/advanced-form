<?php

namespace Qareer\AdvancedForm\Fields;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Qareer\AdvancedForm\Field;
use Qareer\AdvancedForm\Form;

/**
 * Class CollectionField
 *
 * @package Qareer\AdvancedForm\Fields
 */
class CollectionField extends Field implements \IteratorAggregate
{
    /** @var Collection */
    private $fields;

    /** @var int */
    private $currentIndex = 0;

    /** @var bool */
    private $showTemplateAsAttribute = false;

    public function __construct(Form $form, $name, $value = null, $showTemplateAsAttribute = false)
    {
        parent::__construct($form, $name, $value);

        $this->showTemplateAsAttribute = $showTemplateAsAttribute;

        $this->fields = new Collection();
        $this->fields->offsetSet($this->currentIndex, []);
    }

    /**
     * Add a field
     *
     * @param Field     $field
     * @param string    $inputName
     *
     * @return $this
     */
    private function add(Field $field, $inputName)
    {
        if ($this->fields === null) {
            $this->fields = new Collection();
        }

        if ($this->showTemplateAsAttribute) {
            $tempField = clone  $field;
            $tempField->setAsArray('new_placeholder');

            $field->addClass($tempField->getArrayKey());
            $field->addAttribute('data-template', html_escape($tempField->renderElement()));
        }

        $fields = $this->fields->get($this->currentIndex);
        $fields[$inputName] = $field;

        $this->fields->offsetSet($this->currentIndex, $fields);

        return $this;
    }

    /**
     * Add a field with type TextField
     *
     * @param string    $name
     *
     * @return TextField
     */
    public function addText($name)
    {
        $input = new TextField($this->form, $this->name."[{$name}]");
        $input->setAsArray('new_'.md5($this->currentIndex));

        $this->add($input, $name);

        return $input;
    }

    /**
     * Add a field with type SelectField
     *
     * @param string            $name
     * @param array|Arrayable   $options
     *
     * @return SelectField
     */
    public function addSelect($name, $options)
    {
        $options = is_array($options)
            ? $options
            : ($options instanceof Arrayable ? $options->toArray() : []);

        $select = new SelectField($this->form, $this->name."[{$name}]");
        $select->setOptions($options);
        $select->setAsArray('new_'.md5($this->currentIndex));

        $this->add($select, $name);

        return $select;
    }

    /**
     * Add a field with type FileField
     *
     * @param string    $name
     *
     * @return FileField
     */
    public function addFile($name)
    {
        $input = new FileField($this->form, $this->name."[{$name}]");
        $input->setAsArray('new_'.md5($this->currentIndex));

        $this->form->setEncType(Form::FORM_ENCTYPE_MULTIPART);

        $this->add($input, $name);

        return $input;
    }

    /**
     * Add a field with type RadioField
     *
     * @param string    $name
     * @param mixed     $value
     *
     * @return RadioField
     */
    public function addRadio($name, $value)
    {
        $input = new RadioField($this->form, $this->name."[{$name}]", $value);
        $input->setAsArray('new_'.md5($this->currentIndex));

        $this->add($input, $name);

        return $input;
    }

    /**
     * Add a field with type TextareaField
     *
     * @param string    $name
     *
     * @return TextareaField
     */
    public function addTextarea($name)
    {
        $input = new TextareaField($this->form, $this->name."[{$name}]");
        $input->setAsArray('new_'.md5($this->currentIndex));

        $this->add($input, $name);

        return $input;
    }

    /**
     * Add a collection field
     *
     * @param string $name
     *
     * @return CollectionField
     */
    public function addCollection($name)
    {
        $input = new CollectionField($this->form, $this->name."[]{$name}");

        $this->add($input, $name);

        return $input;
    }

    /**
     * @inheritDoc
     *
     * Since CollectionField as only a container, do nothing
     */
    public function setAsArray($arrayKey)
    {
        return $this;
    }

    /** {@inheritDoc} */
    public function handle($postValues)
    {
        $values = [];

        $this->resolveBindings();

        foreach ($this->fields as $index => $fields) {
            $values[$index] = [];

            foreach ($fields as $fieldRealName => $field) {
                $values[$index][$fieldRealName] = $field->handle(g(g($postValues, $fieldRealName), $field->getArrayKey()));
            }
        }

        return $values;
    }

    /** {@inheritDoc} */
    public function getType()
    {
        return 'collection';
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return false;
    }

    /** {@inheritDoc} */
    public function getIterator()
    {
        $this->resolveBindings();

        return $this->fields->getIterator();
    }

    /** {@inheritDoc} */
    public function __invoke()
    {
        return $this->getIterator();
    }

    /**
     * Resolves this collection field bindings if not resolved
     *
     * @return void
     */
    private function resolveBindings()
    {
        if ($this->isBound() && !$this->isRendered) {
            // since it's a Collection, we're expecting an array of QModel
            $models = call_user_func($this->boundField, $this->boundModel);

            // resets the items
            $currentFields      = $this->fields;
            $fieldsTemplate     = $currentFields->toArray();
            $fieldsTemplate     = reset($fieldsTemplate);

            // creates a new empty collection
            $this->fields       = new Collection();
            $this->currentIndex = 0;

            foreach ($models as $model) {
                // sets the current index as the model's id if exists
                $this->currentIndex = $model->id ?: $this->currentIndex++;

                $modelAttributes = $model->getAttributes();
                $clonedFields    = [];

                foreach ($fieldsTemplate as $fieldName => $field) {
                    // shallow clone the field
                    $clonedField = clone $field;

                    // the fields name will be in html string notation with this collection as its parent
                    //
                    // Eg: collection[field]
                    //
                    // find it's real name by scrapping against this collection name
                    $clonedFieldRealName = $clonedField->getName();

                    $opening    = strpos($clonedFieldRealName, $this->name.'[');
                    $closing    = strpos($clonedFieldRealName, ']', $opening);

                    $clonedFieldRealName = substr($clonedFieldRealName, 0, $closing).substr($clonedFieldRealName, $closing + 1);
                    $clonedFieldRealName = str_replace($this->name.'[', '', $clonedFieldRealName);

                    $clonedField->setAsArray($model->id);
                    $clonedField->setBoundModel($model);

                    if ($field->getBoundField() !== null) {
                        // if the field is already bound, pass it
                        $clonedField->setBoundField($field->getBoundField());
                    } elseif (array_key_exists($clonedFieldRealName, $modelAttributes)) {
                        // try to resolve the binding to it's model's attribute
                        $clonedField->setBoundField($clonedFieldRealName);
                    }

                    // build an array with it's real name to allow natural use in template engine
                    $clonedFields[$clonedFieldRealName] = $clonedField;
                }

                $this->fields->offsetSet($this->currentIndex, $clonedFields);
            }

            // if no field is created, pass the saved template back
            if ($this->fields->count() === 0) {
                $this->fields = $currentFields;
            }
        }
    }
}