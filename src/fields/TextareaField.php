<?php

namespace Qareer\AdvancedForm\Fields;

use Qareer\AdvancedForm\Field;

/**
 * Class TextareaField
 *
 * @package Qareer\AdvancedForm\Fields
 */
class TextareaField extends Field
{
    /** {@inheritDoc} */
    public function getType()
    {
        return 'textarea';
    }

    protected function renderInner()
    {
        return $this->getValue();
    }

    /** {@inheritDoc} */
    public function isSelfClosing()
    {
        return false;
    }
}