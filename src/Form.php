<?php

namespace Qareer\AdvancedForm;

use Qareer\AdvancedForm\Fields\CheckboxField;
use Qareer\AdvancedForm\Fields\CollectionField;
use Qareer\AdvancedForm\Fields\EmailField;
use Qareer\AdvancedForm\Fields\NumberField;
use Qareer\AdvancedForm\Fields\FileField;
use Qareer\AdvancedForm\Fields\HiddenField;
use Qareer\AdvancedForm\Fields\MultipleFileField;
use Qareer\AdvancedForm\Fields\MultiSelectField;
use Qareer\AdvancedForm\Fields\PasswordField;
use Qareer\AdvancedForm\Fields\RadioField;
use Qareer\AdvancedForm\Fields\SelectField;
use Qareer\AdvancedForm\Fields\TextareaField;
use Qareer\AdvancedForm\Fields\TextField;
use Illuminate\Support\Collection;
use Valitron\Validator;

include_once(__DIR__.DIRECTORY_SEPARATOR.'helpers.php');

/**
 * Class Form
 *
 * @package Qareer\AdvancedForm
 */
class Form
{
    // autocomplete constants
    const AUTOCOMPLETE_COMPANY   = 'company_name';
    const AUTOCOMPLETE_CITY      = 'city_name';
    const AUTOCOMPLETE_JOB_TITLE = 'job_title';

    // autocomplete constants
    const AUTOCORRECT_COMPANY   = 'company_name';
    const AUTOCORRECT_CITY      = 'city_name';
    const AUTOCORRECT_MAJOR     = 'major';
    const AUTOCORRECT_JOB_TITLE = 'job_title';

    // form enctype constants
    const FORM_ENCTYPE_URL_ENCODED = 'application/x-www-form-urlencoded';
    const FORM_ENCTYPE_MULTIPART   = 'multipart/form-data';
    const FORM_ENCTYPE_TEXT_PLAIN  = 'text/plain';

    // form method constants
    const FORM_METHOD_POST = 'post';
    const FORM_METHOD_GET  = 'get';

    /** @var string */
    protected $formName;

    /** @var array */
    protected $validatableFields = [];

    /** @var string */
    protected $enableJavascriptValidation = false;

    /** @var array */
    protected $formOptions = [
        'action'       => '',
        'class'        => [],
        'method'       => self::FORM_METHOD_POST,
        'enctype'      => self::FORM_ENCTYPE_URL_ENCODED,
        'autocomplete' => 'off',
        'autocorrect'  => 'off',
    ];

    protected $properties;

    /** @var /Valitron/Validator */
    protected $validation;

    /** @var array */
    protected $postValues;

    /** @var array */
    protected $fields;

    /** @var string */
    protected $fieldIdPrefix;

    /** @var array */
    protected $errors;

    /** @var string */
    protected $errorWrapper;

    /** @var string */
    public $labelWrapper;

    /** @var string */
    public $groupWrapper;

    /** @var string */
    public $hasErrorClass = 'has-error';

    /** @var array */
    protected $booleanFields = [
        'readonly', 'disabled', 'selected', 'checked'
    ];

    /** @var array */
    protected $clientValidatableRules = [
        'required'  => 'required',
        'email'     => 'email',
        'url'       => 'url',
        'numeric'   => 'number',
        'lengthMin' => 'minlength',
        'lengthMax' => 'maxlength',
        'min'       => 'min',
        'max'       => 'max'
    ];

    /** @var Array */
    public $clientUnvalidatableField = [
        'RadioField',
        'MultipleFileField',
        'CollectionField',
        'MultiSelectField',
        'MultipleFileField'
    ];

    /** @var Eloquent */
    protected $model;

    /** @var bool */
    private $isOpened = false;

    /** @var bool */
    private $isClosed = false;

    /** @var string */
    private $errorLang = 'en';

    /** @var array */
    private $langMessages = null;

    /** @var string */
    private $errorLangDir = __DIR__.'/lang';

    public function __construct($formAction = null, $properties = [])
    {
        if ($formAction) {
            $this->setAction($formAction);
        }

        $this->setFormName($this->generateRandomString());
        $this->properties   = $properties;
        $this->errorWrapper = $this->getDefaultErrorWrapper();
    }

    /**
     * @param array $fields
     *
     * @return $this
     */
    public function setValidatableField($fields = [])
    {
        $this->validatableFields = $fields;

        return $this;
    }

    /**
     * set error language
     * @param string $lang country code for language
     */
    public function setErrorLang($lang) {
        $this->errorLang = $lang;

        return $this;
    }

    /**
     * set error language directory
     * @param string $dir path of language folder
     */
    public function setErrorLangDir($dir) {
        $this->errorLangDir = $dir;

        return $this;
    }

    /**
     * fill fields value
     *
     * @param array | $data
     *
     * @return this
     */
    public function fill($data)
    {
        foreach ($this->fields as $fieldName => &$fields) {
            if (isset($data[$fieldName])) {
                array_walk($fields, function (Field &$field) use ($data, $fieldName) {
                    $field->setValue($data[$fieldName]);
                });
            }
        }

        return $this;
    }

    /**
     * Handle the input
     *
     * @param array | $input
     *
     * @return mixed
     */
    public function handle($input)
    {
        $this->postValues = $input;
        
        // ToDo Sam, listen if rollback adjust accordingly
        if ($this->postValues) {

            // key : nama field 
            // value : value yang udah di resolve 
            //    e.g : radio > yang di check doang 
            $values = [];
            // loop field
            Validator::langDir($this->errorLangDir);
            Validator::lang($this->errorLang);
            $validation = new Validator($this->postValues);
            foreach ($this->fields as $fieldName => $fields) {
                // if validatableField exists and field not in the set, skip
                if ($this->validatableFields && !in_array($fieldName, $this->validatableFields)) {
                    continue;
                }

                array_walk($fields, function (Field $field) use (&$values, &$validation) {
                    $fieldName = $field->getName();

                    // validation rules
                    foreach ($field->getRules() as $rule) {
                        if (count($rule)) {
                            $message = null;
                            foreach ($rule as $key => $arg) {
                                if (is_array($arg) && isset($arg['message'])) {
                                    $message = $arg['message'];
                                    unset($rule[$key]);
                                }
                            }
                            array_splice( $rule, 1, 0, $fieldName );
                            reset($rule);
                            call_user_func_array([$validation, 'rule'], $rule);
                            if ($message !== null) {
                                call_user_func_array([$validation, 'message'], [$message]);
                            }
                        }
                    }

                    // special case for radio
                    // because it has same name
                    if ($field->getType() === 'radio') {

                        // check in $values if this $fieldName already set 
                        $previousValue = g($values, $fieldName, null);

                        // if not set, then iterate to next RadioField 
                        if ($previousValue === null || $previousValue === false) {
                            $values[$fieldName] = $field->handle(g($this->postValues, $fieldName));
                        }
                        // handling other than radio field
                    } else {
                        // get result from Field->handle() for each field
                        // using $this->postValue[$fieldName] as an input
                        if (strpos($fieldName, '[]')) {
                            $nonArrayName          = str_replace('[]', '', $fieldName);
                            $values[$nonArrayName] = $field->handle(g($this->postValues, $nonArrayName));
                        } else {
                            $values[$fieldName] = $field->handle(g($this->postValues, $fieldName));
                        }
                    }
                });
            }
            $this->validation = $validation;
            
            return $this->validation->validate() ? $values : null;
        }

        return null;
    }

    /**
     * Get the form bound model
     *
     * @return Eloquent
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the form bound model
     *
     * @param Eloquent $model
     *
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;

        $this->resolveBinding();

        return $this;
    }

    /**
     * Create a binding based on exact match between model attribute name and field name
     *
     * @return void
     */
    public function resolveBinding()
    {
        if ($model = $this->getModel()) {
            $modelAttributes = $model->getAttributes();

            foreach ($this->fields as $fieldName => $fields) {
                array_walk($fields, function (Field $field) use ($model, $modelAttributes) {
                    $field->setBoundModel($model);

                    if (array_key_exists($field->getName(), $modelAttributes) && !$field->getBoundField()) {
                        $field->setBoundField($field->getName());
                    }
                });
            }
        }
    }

    /**
     * Get passed post value
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getPostValue($name = '')
    {
        return g($this->postValues, $name);
    }

    public function group($field) {
        $trans = [
            ':has_error_class' => $this->hasError($field) ? $this->hasErrorClass : '',
            ':label' => $this->field($field)->label(),
            ':input' => $this->field($field)->render(),
            ':error' => $this->error($field)
        ];

        $className = last(explode('\\', get_class($this->field($field))));
        if ($this->usingJavascriptValidation() && !in_array($className, $this->clientUnvalidatableField)){
            $trans[':has_error_class'] = '" v-bind:class="{ \''.$this->hasErrorClass.'\': (('.$this->getFormName().'.$submitted && '.$this->getFormName().'.$error.'.$field.') || (!'.$this->getFormName().'.$submitted && initialError.'.$field.')) }';
        }
        $groupWrapper = ($this->groupWrapper) ?: $this->getDefaultGroupWrapper();
        
        return strtr($groupWrapper, $trans);

    }

    /**
     * Set the form method
     *
     * @param string $method
     *
     * @return $this
     */
    public function setMethod($method)
    {
        $this->formOptions['method'] = $method;

        return $this;
    }

    /**
     * Get the form method
     *
     * @return string
     */
    public function getMethod()
    {
        return g($this->formOptions, 'method');
    }

    /**
     * Set the form action URL
     *
     * @param string $url
     *
     * @return $this
     */
    public function setAction($url)
    {
        $this->formOptions['action'] = $url;

        return $this;
    }

    /**
     * Get the form action
     *
     * @return string
     */
    public function getAction()
    {
        return g($this->formOptions, 'action');
    }

    /**
     * Set the form enctype
     *
     * @param string $encType
     *
     * @return $this
     */
    public function setEncType($encType)
    {
        $this->formOptions['enctype'] = $encType;

        return $this;
    }

    /**
     * Set field values
     *
     * @param array $values
     *
     * @return $this
     */
    public function setValues(array $values)
    {
        foreach ($this->fields as $name => $fields) {
            $fieldValue = g($values, $name);

            array_walk($fields, function (Field $field) use ($fieldValue) {
                $field->setValue($fieldValue);
            });
        }

        return $this;
    }

    /**
     * Get the current form style
     *
     * @return Style
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Set the current form style
     *
     * @param Style $style
     *
     * @return $this
     */
    public function setStyle(Style $style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get validation errors
     *
     * @return array
     */
    public function getErrors()
    {
        if (!$this->errors && $this->validation instanceof Validator) {
            $this->errors = $this->validation->errors();
        }

        return $this->errors;
    }

    /**
     * Add a field
     *
     * @param Field $field
     *
     * @return $this
     */
    public function add(Field $field)
    {
        $fieldName = $field->getName();

        if (!isset($this->fields[$fieldName])) {
            $this->fields[$fieldName] = [];
        }

        $this->fields[$fieldName][] = $field;

        return $this;
    }
    
    /**
     * add attributes to input
     *
     * @param string $name          name of input field
     * @param array  $attributes    array input attr for it's dom element
     **/
    private function addFieldAttributes($field, $attributes, $override = false) 
    {
        if (is_array($attributes) && count($attributes)) {
            array_walk($attributes, function($value, $name) use ($field, $override) {
                if ($override){
                    $field->setAttribute($name, $value);
                } else {
                    $field->addAttribute($name, $value);
                }
            });
        }

        return $field; 
    }

    /**
     * Add a field with type TextField
     *
     * @param string $name
     * @param array $attributes
     *
     * @return TextField
     */
    public function addText($name, $attributes = false)
    {
        $input = new TextField($this, $name);
        $this->addFieldAttributes($input, $attributes);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type NumberField
     *
     * @param string $name
     * @param array $attributes
     *
     * @return TextField
     */
    public function addNumber($name, $attributes = false)
    {
        $input = new NumberField($this, $name);
        $this->addFieldAttributes($input, $attributes);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type HiddenField
     *
     * @param string $name
     * @param array $attributes
     *
     * @return HiddenField
     */
    public function addHiddenText($name, $attributes = false)
    {
        $input = new HiddenField($this, $name);
        $this->addFieldAttributes($input, $attributes);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type EmailField
     *
     * @param string $name
     * @param array $attributes
     *
     * @return EmailField
     */
    public function addEmail($name, $attributes = false)
    {
        $input = new EmailField($this, $name);
        $this->addFieldAttributes($input, $attributes);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type PasswordField
     *
     * @param string $name
     * @param array $attributes
     *
     * @return PasswordField
     */
    public function addPassword($name, $attributes = false)
    {
        $input = new PasswordField($this, $name);
        $this->addFieldAttributes($input, $attributes);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type SelectField
     *
     * @param string          $name
     * @param array|Arrayable $options
     * @param array $attributes
     *
     * @return SelectField
     */
    public function addSelect($name, $options, $attributes = false)
    {
        $options = is_array($options)
            ? $options
            : ($options instanceof Arrayable ? $options->toArray() : []);

        $select = new SelectField($this, $name);
        $this->addFieldAttributes($select, $attributes);
        $select->setOptions($options);

        $this->add($select);

        return $select;
    }

    /**
     * Add a field with type MultipleSelectField
     *
     * @param string          $name
     * @param array|Arrayable $options
     * @param array $attributes
     *
     * @return SelectField
     */
    public function addMultipleSelect($name, $options, $attributes = false)
    {
        $options = is_array($options)
            ? $options
            : ($options instanceof Arrayable ? $options->toArray() : []);

        $select = new MultiSelectField($this, $name);
        $this->addFieldAttributes($select, $attributes);
        $select->setOptions($options);

        $this->add($select);

        return $select;
    }

    /**
     * Add a field with type FileField
     *
     * @param string $name
     * @param array $attributes
     *
     * @return FileField
     */
    public function addFile($name, $attributes = false)
    {
        $input = new FileField($this, $name);
        $this->addFieldAttributes($input, $attributes);

        $this->setEncType(self::FORM_ENCTYPE_MULTIPART);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type MultipleFileField
     *
     * @param string $name
     * @param array $attributes
     *
     * @return FileField
     */
    public function addMultipleFile($name, $attributes = false)
    {
        $input = new MultipleFileField($this, $name);
        $this->addFieldAttributes($input, $attributes);
        
        $this->setEncType(self::FORM_ENCTYPE_MULTIPART);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type RadioField
     *
     * @param string $name
     * @param mixed  $value
     * @param array $attributes
     *
     * @return RadioField
     */
    public function addRadio($name, $value, $attributes = false)
    {
        $input = new RadioField($this, $name, $value);
        $this->addFieldAttributes($input, $attributes);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type CheckboxField
     *
     * @param string $name
     * @param mixed  $value
     * @param array $attributes
     *
     * @return CheckboxField
     */
    public function addCheckbox($name, $value, $attributes = false)
    {
        $input = new CheckboxField($this, $name, $value);
        $this->addFieldAttributes($input, $attributes);

        $this->add($input);

        return $input;
    }

    /**
     * Add a field with type TextareaField
     *
     * @param string $name
     * @param array $attributes
     *
     * @return TextareaField
     */
    public function addTextarea($name, $attributes = false)
    {
        $input = new TextareaField($this, $name);
        $this->addFieldAttributes($input, $attributes);

        $this->add($input);

        return $input;
    }

    /**
     * Add a collection field
     *
     * @param string $name
     * @param array $attributes
     *
     * @return CollectionField
     */
    public function addCollection($name, $showTemplateAsAttribute = false)
    {
        $input = new CollectionField($this, $name, null, $showTemplateAsAttribute);

        $this->add($input);

        return $input;
    }

    /**
     * escape html 
     * @param  mixed  $var           
     * @param  boolean $double_encode 
     * @return string                  
     */
    public function html_escape($var, $double_encode = TRUE)
	{
		if (empty($var))
		{
			return $var;
		}
		
		if (is_array($var))
		{
			return array_map(function($value) use ($double_encode){
                return $this->html_escape($value, $double_encode);
            } , $var);
		}

		return htmlspecialchars($var, ENT_QUOTES, 'UTF-8', $double_encode);
	}

    function addRule($name, $callback, $message) {
        Validator::addRule($name, $callback, $message);

        return $this;
    }

    /**
     * Enable javascript 
     *
     * @return $this
     */
    public function enableJavascriptValidation()
    {
        $this->enableJavascriptValidation = true;

        return $this;
    }

    /**
     * Check is using javacript validation or not
     *
     * @return string
     */
    public function usingJavascriptValidation()
    {
        return $this->enableJavascriptValidation;
    }

    /**
     * Parse attributes to <key>=<value> string syntax
     *
     * @param array $attributes
     *
     * @return array
     */
    public function parseAttributes(array $attributes)
    {
        $stringAttributes = [];

        foreach ($attributes as $attribute => $value) {
            $attribute = $this->html_escape($attribute);
            $value     = $this->html_escape($value);

            if (in_array($attribute, $this->booleanFields)) {
                if (filter_var($value, FILTER_VALIDATE_BOOLEAN) !== false) {
                    $stringAttributes[] = "{$attribute}";
                }
            } elseif ($value === null) {
                $stringAttributes[] = "{$attribute}";
            } else {
                if (is_array($value)) {
                    $value = implode(' ', $value);
                }

                $stringAttributes[] = "{$attribute}='{$value}'";
            }
        }

        return $stringAttributes;
    }

    /**
     * Parse attributes to html attribute friendly
     *
     * @param array $attributes
     *
     * @return string
     */
    public function parseAttributesAsString(array $attributes)
    {
        return implode(' ', $this->parseAttributes($attributes));
    }

    public function addClasses(array $classes)
    {
        $this->formOptions['class'] = array_merge(
            $this->formOptions['class'], $classes
        );

        return $this;
    }

    public function addAttribute($field, $value)
    {
        $this->formOptions[$field] = $value;

        return $this;
    }

    public function open($url = null, $model= null,  $attrs = [])
    {
        if ($this->isOpened) {
            return false;
        }

        if ($url) {
            $this->setAction($url);
        }

        if ($model) {
            $this->setModel($model);
        }

        if ($this->enableJavascriptValidation) {
            $this->formOptions['novalidate']      = null;
            $this->formOptions['v-form']          = null;
            $this->formOptions['@submit'] = "onSubmit";
        }

        $formAttributes = array_merge($this->formOptions, ['name' => $this->formName], $attrs);
        $form = '';
        if ($this->enableJavascriptValidation) {
            $form .= '<div id="wrapper-form-'.$this->getFormName().'">'; 
        }

        $attributes = $this->parseAttributesAsString($formAttributes);

        $this->isOpened = true;
        $this->isClosed = false;

        $form .= "<form {$attributes}>";

        return $form; 
    }

    /**
    * @param $length 
    *
    * @return string
    */
    private function generateRandomString($length = 5) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param $formName
     *
     * @return $this
     */
    public function setFormName($formName)
    {
        $this->formName = $formName;

        return $this;
    }

    /**
     * close form tag
     * 
     * @return string
     */
    public function close()
    {
        if ($this->isClosed) {
            return false;
        }

        $this->isOpened = false;
        $this->isClosed = true;


        $form = "</form>";
        if ($this->usingJavascriptValidation()) {
            $value = [];
            $initialError = [];
            foreach($this->fields as $fieldName => $field) {
                $value[$fieldName]        = $this->getPostValue($fieldName);
                $initialError[$fieldName] = $this->hasError($fieldName);
                $className                = last(explode('\\', get_class($this->field($fieldName))));
                if ($value[$fieldName] === null && $className == 'SelectField') {
                    $value[$fieldName] = g(array_keys($this->field($fieldName)->getOptions()), 0);
                }
            }
            $form .= 
                '</div>
                <script>
                    var vm = new Vue({
                        el: \'#wrapper-form-'. $this->getFormName() .'\',
                        data: {
                            '. $this->getFormName() .': {},
                            model: '.json_encode($value).',
                            initialError: '.json_encode($initialError).'
                        },
                        methods: {
                            onSubmit: function (e) {
                                if (!this.'. $this->getFormName() .'.$valid){
                                    e.preventDefault();
                                    var obj = Object.keys(this.'. $this->getFormName() .'.$error)[0];
                                    document.querySelector(\'#wrapper-form-'. $this->getFormName() .' [name="\'+obj+\'"]\').focus();
                                }
                            }
                        }
                    });
                </script>';
        }

        return $form;
    }

    /**
     * Get field by name
     *
     * @param string $name
     * @param string $value
     *
     * @return Field
     */
    public function field($name, $value = null, $attributes = null)
    {
        $fields = g($this->fields, $name);

        if (!is_null($value) && $fields) {
            $fields = array_filter($fields, function (Field $field) use ($value) {
                return $field->getValue() === $value;
            });
        }

        if (!$fields) {
            return null;
        } else {
            $field = reset($fields);
            $this->addFieldAttributes($field, $attributes, true);
            
            return $field;
        }
    }

    /**
     * Set wrapper for field error
     *
     * If an error occurred, the error will be passed to this wrapper
     *
     * @param \Closure $callable
     *
     * @return $this
     */
    public function setErrorWrapper($wrapper)
    {
        $this->errorWrapper = $wrapper;

        return $this;
    }

    /**
     * Set wrapper for field error
     *
     * If an error occurred, the error will be passed to this wrapper
     *
     * @param \Closure $callable
     *
     * @return $this
     */
    public function setGroupWrapper($wrapper)
    {
        $this->groupWrapper = $wrapper;

        return $this;
    }

    public function setHasErrorClass($class) {
        $this->hasErrorClass = $class;

        return $this; 
    }

    /**
     * get error message by field name
     * 
     * @param  string $name
     * 
     * @return \Collection 
     */
    public function errorMessages($name) {
        $message = g($this->getErrors(), $name);
        $message = collect($message);

        return $message;
    }

    /**
     * get first error message by field name
     * 
     * @param  string $name 
     * 
     * @return string
     */
    public function errorMessage($name)
    {
        $messages = $this->errorMessages($name);
        $message  = $messages->first();

        return $message;
    }

    /**
     * check error in field after validation run
     * @param  string  $name 
     * @return boolean
     */
    public function hasError($name) 
    {
        return $this->errorMessage($name) ? true : false; 
    }

    public function addError($field, $message) 
    {
        $error = $this->getErrors();
        if (!isset($error[$field])) {
            $error[$field] = [$message];
        } else {
            $error[$field][] = $message;
        }
        $this->errors = $error;

        return $this;
    }

    /**
     * get string of error message with it's wrapper
     * 
     * @param  string $name
     * 
     * @return string       
     */
    public function error($name)
    {
        $errorMessage = $this->errorMessage($name);
        if ($this->langMessages === null) {
            $langFile = rtrim($this->errorLangDir, '/') . '/' . $this->errorLang . '.php';
            $this->langMessages = [];
            if (stream_resolve_include_path($langFile) ) {
                $this->langMessages = include $langFile;
            }
        }

        $className = last(explode('\\', get_class($this->field($name))));
        if ($this->usingJavascriptValidation() && !in_array($className, $this->clientUnvalidatableField)){
            $trans = [
                ':field' => $name, 
                ':error' => 
                    '<span v-if="!'.$this->getFormName().'.$submitted">'.$errorMessage.'</span>
                    <span v-if="'.$this->getFormName().'.$submitted">'
            ];
            $fieldName = ucwords(str_replace('_', ' ', $name));

            foreach($this->field($name)->getRules() as $rule) {
                if (isset($this->clientValidatableRules[g($rule, 0)])){
                    $ruleName = $this->clientValidatableRules[g($rule, 0)];
                    $errorMessage = '{field} '.g($this->langMessages, g($rule, 0));
                    unset($rule[0]);
                    foreach ($rule as $k => $v) {
                        if (is_array($v) && isset($v['message'])) {
                            $errorMessage = $v['message'];
                            unset($rule[$k]);
                            break;
                        }
                    }
                    $errorMessage = str_replace('{field}', $fieldName, $errorMessage);
                    if (count($rule)) {
                        $errorMessage = vsprintf($errorMessage, $rule);
                    }
                    $trans[':error'] .= '<span class="text-danger" v-if="'.$this->getFormName().'.'.$name.'.$error.'.$ruleName.'">'.$errorMessage.'</span>';
                }
            }
            $trans[':error'] .= '</span>';
        } else { 
            $trans = [':error' => $errorMessage, ':field' => $name];
        }
        $errorWrapper = ($this->errorWrapper) ?: $this->getDefaultErrorWrapper();
        return strtr($errorWrapper, $trans);
    }


    public function __call($method, $params)
    {
        $args = array_merge([$method], $params);

        return call_user_func_array([$this, 'field'], $args);
    }

    /**
     * @return \String
     */
    public function getDefaultErrorWrapper()
    {
        return '<span class="label label-danger validation-error">:error</span>';
    }

    /**
     * set labelWrapper
     * 
     * @param string $wrapper
     */
    public function setLabelWrapper($wrapper) {
        $this->labelWrapper = $wrapper;

        return $this;
    }

    /**
     * @return \String
     */
    public function getDefaultLabelWrapper()
    {
        return '<label for=":for" :attributes>:label</label>';
    }

    /**
     * @return \String
     */
    public function getDefaultGroupWrapper()
    {
        return '
            <div class="form-group :has_error_class">
                :label
                :input
                :error
            </div>';
    }

    /**
     * @return string
     */
    public function getFormName()
    {
        return $this->formName;
    }
    
    /**
     * @param $prefix
     *
     * @return $this
     */
    public function setFieldIdPrefix($prefix)
    {
        $this->fieldIdPrefix = $prefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getFieldIdPrefix()
    {
        return $this->fieldIdPrefix;
    }
}