<?php
require 'vendor/autoload.php';  
 
use Illuminate\Database\Capsule\Manager as Capsule;  
 
$capsule = new Capsule; 
 
$capsule->addConnection(array(
    'driver'    => 'mysql',
    'host'      => 'testing.qerja.com',
    'database'  => 'qolega',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => ''
));
 
$capsule->bootEloquent();

class Admin extends Illuminate\Database\Eloquent\Model {
    public $table = 'admins';
    public $timestamps = false;
}

$admin = Admin::find(1);

// init form
$form = new Qareer\AdvancedForm\Form();
$form->setErrorWrapper('<span class="help-block">:error</span>')       // set error wrapper [OPTIONAL]
    ->setLabelWrapper('<label class="control-label" for=":for">:label</label>')    // set label wrapper [OPTIONAL] 
    ->setErrorLang('id');     // set form lang [OPTIONAL]

// start create form input component
$form->addHiddenText('id')->setValue(10);

$form->addText('name', ['class' => 'form-control'])
    ->addRule('required');

$form->addText('username', ['class' => 'form-control'])
    ->addRule('required')
    ->addRule('alphaNum');

$form->addPassword('password', ['class' => 'form-control'])
    ->addRule('required')
    ->addRule('lengthMin', 6, ['message' => 'Minimal harus ENAM karakter lhoo.']);
// end create form input component

// post request action
$successMessage = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //$_POST['photo'] = $_FILES['photo']['name'];
    if ($data = $form->handle($_POST)) {
        $successMessage = "Thank You for filling form correctly!!";
    }
}

?>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1>Form Example</h1>
                    <?php 
                    if ($successMessage){
                        echo '<div class="alert alert-success" role="alert">'.$successMessage.'</div>';
                    }
                    ?>
                    <?= $form->open(null, $admin)?>
                        <?= $form->group('name')?>
                        <?= $form->group('username')?>
                        <?= $form->group('password')?>
                        <button type="submit" class="btn btn-default">Submit</button>
                    <?= $form->close()?>
                </div>
            </div>
            <!-- Latest compiled and minified JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        </div>
    </body>
</html>