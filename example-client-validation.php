<?php
require('vendor/autoload.php');

// init form
$form = new Qareer\AdvancedForm\Form();
$form->setErrorWrapper('<span class="help-block">:error</span>')       // set error wrapper [OPTIONAL]
    ->setLabelWrapper('<label class="control-label" for=":for">:label</label>')    // set label wrapper [OPTIONAL] 
    ->setErrorLang('id')     // set form lang [OPTIONAL]
    ->enableJavascriptValidation()          // enable client side validation
    ->setErrorLangDir(__DIR__.'/src/lang');      // set lang source directory [OPTIONAL]

// start create form input component
$form->addHiddenText('id')->setValue(10);

$form->addEmail('email', ['class' => 'form-control'])
    ->addRule('required')
    ->addRule('email');

$form->addPassword('password', ['class' => 'form-control'])
    ->addRule('required')
    ->addRule('lengthMin', 6, ['message' => 'Minimal harus ENAM karakter lhoo.']);

$form->addPassword('password_confirm', ['class' => 'form-control'])
    ->addRule('required')
    ->addRule('equals', 'password')
    ->addRule('lengthMin', 6);

$form->addText('age', ['class' => 'form-control'])
    ->addRule('required')
    ->addRule('numeric')
    ->addRule('max', 60)
    ->addRule('min', 18, ['message' => 'Umur harus lebih dari 18 tahun']);

$form->addTextarea('description', ['class' => 'form-control', 'rows' => 5]);

$form->addSelect('gender', ['M' => 'Male', 'F' => 'Female'], ['class' => 'form-control'])
    ->addRule('in', ['M', 'F'])
    ->addRule('required');

$interests = [
    'Tech',
    'Nature',
    'Education',
    'Politics',
    'Animal'
];
$form->addMultipleSelect('interest', $interests, ['class' => 'form-control'])
    ->addRule('array')
    ->addRule('required');

$form->addRadio('marital_status', 'single')
    ->addRule('required');
$form->addRadio('marital_status', 'married');
$form->addRadio('marital_status', 'divorced');

// end create form input component

// post request action
$successMessage = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //$_POST['photo'] = $_FILES['photo']['name'];
    if ($data = $form->handle($_POST)) {
        $successMessage = "Thank You for filling form correctly!!";
    }
}

?>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./assets/js/vue.js"></script>
        <script src="./assets/js/vue-form.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1>Form Example</h1>
                    <?php 
                    if ($successMessage){
                        echo '<div class="alert alert-success" role="alert">'.$successMessage.'</div>';
                    }
                    ?>
                    <?= $form->open()?>
                        <?= $form->group('email')?>
                        <?= $form->group('password')?>
                        <?= $form->group('password_confirm')?>
                        <?= $form->group('age')?>
                        <?= $form->group('description')?>
                        <?= $form->group('gender')?>  
                        <div class="form-group <?= $form->hasError('interest') ? 'has-error' : '' ?>">
                            <?= $form->interest()->label()?>
                            <?= $form->interest()?>
                            <?= $form->error('interest')?>
                        </div>
                        <div class="form-group <?= $form->hasError('marital_status') ? 'has-error' : '' ?>">
                            <?= $form->field('marital_status')->label()?></br>
                            <label><?= $form->marital_status('single')?> Single </label> &nbsp;
                            <label><?= $form->marital_status('married')?> Married </label> &nbsp;
                            <label><?= $form->marital_status('divorced')?> Divorced </label> &nbsp;
                            <?= $form->error('marital_status')?>
                        </div>                      
                        <button type="submit" class="btn btn-default">Submit</button>
                    <?= $form->close()?>
                </div>
            </div>
        </div>
    </body>
</html>