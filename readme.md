# Advanced Form

## Basic Usage

Create your form object and specify your fields.
```php
$form = new Qareer\AdvancedForm\Form();
$form->setErrorWrapper('<span class="help-block">:error</span>')       // set error wrapper [OPTIONAL]
    ->setLabelWrapper('<label class="control-label" for=":for">:label</label>')    // set label wrapper [OPTIONAL] 

$form->addHiddenText('id')->setValue(10);

$form->addUsername('username')
    ->addRule('required');

$form->addEmail('email')
    ->addRule('required')
    ->addRule('email');

// custom error message
$form->addPassword('password')
    ->addRule('required')
    ->addRule('lengthMin', 6, ['message' => 'Minimal harus ENAM karakter lhoo.']);
```

Then render your form.
```html
<h1>Please Fill this Form</h1>
<?= $form->open()?> 
    <?= $form->group('username')?>
    <?= $form->group('email')?>
    <div class="form-group <?= $form->hasError('password') ? 'has-error' : '' ?>">
        <?= $form->field('password')->label(['class' => 'text-success'])?>
        <?= $form->field('password', null, ['required' => null, 'data-bound' => '1000', 'class' => 'form-control input-sm'])?>
        <?= $form->error('password')?>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
<?= $form->close()?>
```

Then handle your post data and validate with form object
```php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($data = $form->handle($_POST)) {
        // validation success, proccess your data here
    } else {
        // validation fail, show your form again with passed data and error message
    }
}
```

## Validation

Validation in this form is using package from this [URL](https://raw.githubusercontent.com/vlucas/valitron/).

#### Built-in Validation Rules

 * `required` - Required field
 * `equals` - Field must match another field (email/password confirmation)
 * `different` - Field must be different than another field
 * `accepted` - Checkbox or Radio must be accepted (yes, on, 1, true)
 * `numeric` - Must be numeric
 * `integer` - Must be integer number
 * `array` - Must be array
 * `length` - String must be certain length
 * `lengthBetween` - String must be between given lengths
 * `lengthMin` - String must be greater than given length
 * `lengthMax` - String must be less than given length
 * `min` - Minimum
 * `max` - Maximum
 * `in` - Performs in_array check on given array values
 * `notIn` - Negation of `in` rule (not in array of values)
 * `ip` - Valid IP address
 * `email` - Valid email address
 * `url` - Valid URL
 * `urlActive` - Valid URL with active DNS record
 * `alpha` - Alphabetic characters only
 * `alphaNum` - Alphabetic and numeric characters only
 * `slug` - URL slug characters (a-z, 0-9, -, \_)
 * `regex` - Field matches given regex pattern
 * `date` - Field is a valid date
 * `dateFormat` - Field is a valid date in the given format
 * `dateBefore` - Field is a valid date and is before the given date
 * `dateAfter` - Field is a valid date and is after the given date
 * `contains` - Field is a string and contains the given string
 * `creditCard` - Field is a valid credit card number
 * `instanceOf` - Field contains an instance of the given class

How to use these validation rules, call method `addRule()` on field object. It has dynamic arguments based on your rules. When you want to add custom message, add **array** variable with **message** as key and your custom message as **value** in your parameters. Here's the example code:

```php
// add rules when create input object
$form->addText('name')
    ->addRule('required');

// add rules to the existed input object
$form->field('email')
    ->addRule('email');

// add rule with custom error message
$form->addText('name')
    ->addRule('required', ['message' => 'Please don\'t forget to fill this']);

// add rule with custom error message and include input label dynamically
$form->addText('name')
    ->addRule('required', ['message' => 'Hey, please fill {field}']);
```

### Custom Validation Rules

When you want to create custom validation rule, you must create it under **form** object. Here's the example:

```php
// custom validation rule
$form->addRule('oneToTen', function($field, $value) {
    return ($value > 0 && $value <= 10);
}, "value must between 1-10");

// add custom rule to field
$form->addText('mark')
    ->addRule('oneToTen');
```

### Validation Language Localization

This validation have build in validation language, you can override it by copying folder **lang** from this directory `./vendor/package_name/src`. 
To change validation language use method `setErrorLang('your_lang')` on top of **form** object. If you want to change it's Language directory you can use method `setErrorLangDir('path/of/your/lang')` on top of **form** object.

### Validation Handling

To handle validation with this form object, use method `handle($input_data)`. This method return null if validation fail and return your input_data if validation pass. Here's the example:

```php
if ($data = $form->handle($_POST)) {
    // validation success, proccess your data here
} else {
    // validation fail, show your form again with passed data and error message
}
```

### Client Side Validation

To enable client side validation using **vuejs**, we must call `enableJavascriptValidation()` method under form object then include file `vue.js` & `vue-form.min` in your html page. 
Not all validation rules are working in client side, only `required`, `maxLength`, `minLength`, `max`, `min`, `numeric`, `email` and `url`. Here's how to use it. 

```php
// create form object and enable client side validation
$form = new Qareer\AdvancedForm\Form();
$form->enableJavascriptValidation();

$form->addEmail('email', ['class' => 'form-control'])
    ->addRule('required')
    ->addRule('email');

$form->addPassword('password', ['class' => 'form-control'])
    ->addRule('required')
    ->addRule('lengthMin', 6, ['message' => 'Minimal harus ENAM karakter lhoo.']);
```
In the view:
```html
<html>
    <head>
        <script src="./assets/js/vue.js"></script>
        <script src="./assets/js/vue-form.js"></script>
    </head>
    <body>
        <?= $form->open()?>
            <?= $form->group('email')?>
            <?= $form->group('password')?>
            <button type="submit" class="btn btn-default">Submit</button>
        <?= $form->close()?>
    </body>
</html>
```



## Eloquent Model Binding

This form object can also binded with Eloquent model. You can set model binding on **form** object or when rendering form open. Here's the example:
```php
// create model
$user = User::find(1);

// bind model in form object
$form->setModel($user);

// or you can bind on rendering form open
echo $form->open('action/route', $user);
```

## View Renderer Method

This section will describe how to render form element in view.

### Group Input

If we want to render complete field with label, error, and it's input. We can use method `group('field_name')`. Here's the example:

```php
$form->group('email');
/*
output:
<div class="form-group">
    <label class="control-label" for="email">Email</label>
    <input id='email' name='email' type='email' class='form-control' />
    <span class="help-block"></span>
</div>
*/
```

### Label

Label value by default is uppercase words of field name. If we want to change it, we can call method `setLabel()` under field object. Here's the example:

```php
// call method in when create form
$form->field('grade')
    ->setLabel('Please input your grade');

// call method when in view then render it
$form->grade()->setLabel('Please input your grade')->label();
```

Rendering form by field is using method `label()` under field object. You can see the example on preview code. 

### Input

To render input you can call directly by it's name as method on the form object or call `field('name')` method on your view. Here's the example:

```php
// call field name directly
$form->email();

// call with field method and passing attributes for input on the third param, second param was used to pass value
$form->field('name', null, ['class' => 'form-control']);

// render input and chained call it's field method
$form->email()->addAttribute('required', true)->render();
```

### Error Message

Error message filled when validation fail. You can get error messages by methods under **form** object.

#### Get Array of Error Message from Field

You can get all error message from single field, it return `Collection` of error messages.
```php
$form->errorMessages('name');
```

#### Get First Error Message of Field

When you want get the first error message you can call method below.
```php
$form->errorMessage('name');
```

#### Get Error Message with Wrapper

When you want to render error message with it's wrapper just call method `error('field')` and if you want to change wrapper template you can use `setErrorWrapper($template)`. In the wrapper template it has 2 variable that can be injected, `:error` and `:field`.
```php
// set custom error message wrapper
$form->setErrorWrapper('<span class="help-block">:error</span>');

// render error message with wrapper
$form->error('email');
```